## Jazz Language
**Jazz** is a dynamically typed,lightweight (maybe)embedable programming language written in Rust.

**JazzVM** is a virtual machine for dynamically typed programming languages where everything is Object

**Jazz** has been developed for fun and as a school project. It is using JazzVM that runs everywhere where Rust compiles.
The parser and compiler is under 2k lines long and VM is also in 2k lines of code(like in **Gravity**)


## Example code
```julia
class Point2 {
    let x = 0;
    let y = 0;

    function display() {
        $System.println("(",this.x," ; ",this.y,")");
        return 0;
    }
    function toString() {
        return $concat("(",this.x," ; ",this.y,")");
    }
    function Point2(bx,by) {
        local res = $clone(this); # yes you need to clone `this` because ownership is bad for now :(
        res.x = bx;
        res.y = by;
        return res;
    }
}


local p = $Point2(2.5,2.5);
p.display();

return;

```

## Features

* dynamic typing
* classes
* basic GC via JazzVM
* Stack based VM

## Special thanks 
**Jazz** was written by using ideas from couple of projects. The VM is fork of [losfair](https://github.com/losfair) HexagonVM.

Syntax inspired by [Marco Bambiny](https://github.com/marcobambini) programming language [Gravity](https://github.com/marcobambini/gravity) and [Lua](https://lua.org)

