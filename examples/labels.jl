local i = 25;
if i < 26 {
    goto if_true;
} else {
    goto if_false;
}


label if_true;
$System.println("true");
goto end;


label if_false;
$System.println("false");
goto end;

label end;

return 1;