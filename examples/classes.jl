class Point2 {
    let x = 0;
    let y = 0;

    function display() {
        $System.println("(",this.x," ; ",this.y,")");
        return 0;
    };

    function toString() {
        result = $concat("(",this.x," ; ",this.y,")");
        this.__as_str__ = result;
        return result;
    }

    function Point2(bx,by) {
        local res = $clone(this);
        res.x = bx;
        res.y = by;
        return res;
    }

    function __add__(y) {
        local result = $clone(this);
        result.x = result.x + y.x;
        result.y = result.y + y.y;
        return result;
    }




}


local p = $Point2(2.5,2.5);
local p2 = $Point2(2.5,2.5);
local p3 = p + p2;
p3.display();

return 1;