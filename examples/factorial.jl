function factorial(num) {
    if num < 2 {
        return num;
    } else {
        return num * $factorial(num - 1);
    }
}

let print = $System.println;

$print($factorial(3));
return 1;