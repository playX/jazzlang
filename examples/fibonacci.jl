function fibonacci(num) {
    if num == 0 {
        return 0;
    }
    if num == 1 {
        return 1;
    }
    
    return $fibonacci(num - 2) + $fibonacci(num - 1);
    
}

$System.println("fibonacci(6) = ",$fibonacci(6));

return 1;