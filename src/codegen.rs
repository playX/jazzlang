use super::builtin_fn;
use super::parser::*;
use jazz_vm::*;
use super::DEBUG;
use std::any::Any;
use std::cell::RefCell;
use std::collections::HashMap;

#[derive(Clone)]
pub struct Enum {
    pub name: String,
    pub fields: RefCell<HashMap<String,usize>>,
}

impl Object for  Enum {
    fn as_any(&self) -> &Any {
        self as &Any
    }

    fn as_any_mut(&mut self) -> &mut Any {
        self as &mut Any
    }

    fn o_clone(&self,_: &mut ObjectPool) -> Value {
        panic!("Cannot clone")
    }

    fn get_children(&self) -> Vec<usize> {
        Vec::new()
    }
    fn typename(&self) -> &str {
        &self.name
    }
    fn initialize(&mut self,_: &mut ObjectPool) {}

    fn get_field(&self, _p: &ObjectPool, name: &str) -> Option<Value> {

        let idx =  *self
                .fields
                .borrow()
                .get(name)
                .expect(&format!("Field not found `{}`", name));

        Some(Value::Int(idx as i64))
    }

}

#[derive(Clone)]
pub struct Class {
    pub name: String,
    pub fields: RefCell<HashMap<String, Value>>,
}

impl Class {
    pub fn new() -> Class {
        Class {
            name: String::new(),
            fields: RefCell::new(HashMap::new()),
        }
    }
}

impl Object for Class {
    fn as_any(&self) -> &Any {
        self as &Any
    }
    fn as_any_mut(&mut self) -> &mut Any {
        self as &mut Any
    }

    fn get_children(&self) -> Vec<usize> {
        Vec::new()
    }
    fn o_clone(&self, p: &mut ObjectPool) -> Value {
        let v = p.allocate(Box::new(self.clone()));
        Value::Object(v)
    }
    fn typename(&self) -> &str {
        &self.name
    }

    fn initialize(&mut self, _pool: &mut ObjectPool) {}

    fn call(&self, m: &mut Machine, _argc: Option<usize>) -> Value {
        let fields = self.fields.borrow();
        let call_field = fields.get(&self.name).expect("Class doesn't have caller");

        let obj = ValueContext::new(&call_field, &m.object_pool).as_object();

        obj.call(m, _argc)
    }

    fn set_field(&self, name: &str, val: Value) {
        self.fields.borrow_mut().insert(name.to_string(), val);
    }

    fn get_field(&self, _p: &ObjectPool, name: &str) -> Option<Value> {
        Some(
            *self
                .fields
                .borrow()
                .get(name)
                .expect(&format!("Field not found `{}`", name)),
        )
    }



    fn call_field(&self, name: &str, m: &mut Machine, _argc: Option<usize>) -> Value {
        let field = self
            .get_field(&m.object_pool, name)
            .expect("Field not found");

        let obj = ValueContext::new(&field, &m.object_pool).as_object();
        obj.call(m, _argc)
    }
}

#[derive(Debug, Clone)]
pub enum UOP {
    Op(Opcode),
    Goto(String),
}

pub struct Compiler {
    pub machine: Machine,
    pub labels: HashMap<String, Option<usize>>,
    pub code: Vec<UOP>,
    pub last_end: String,
}

impl Compiler {
    pub fn new() -> Compiler {
        Compiler {
            machine: Machine::new(),
            labels: HashMap::new(),
            code: vec![],
            last_end: String::new(),
        }
    }

    pub fn new_label_here(&mut self, s: String) {
        self.labels.insert(s, Some(self.code.len()));
    }

    /// Creates a new label wich position is not known.
    pub fn new_empty_label(&mut self) -> String {
        let lab_name = self.labels.len().to_string();
        self.labels.insert(lab_name.clone(), None);
        lab_name
    }

    pub fn compile(&mut self, p: (Vec<Stmt>, Vec<FnDef>, Vec<ClassDef>,Vec<EnumDef>)) -> Vec<Opcode> {
        let mut p = p.clone();
        for fun in p.1.iter() {
            let mut cmpl = Compiler::new();
            let code = cmpl.compile((vec![*fun.clone().body], vec![], vec![],vec![]));
            if unsafe { DEBUG } {
                println!("Printing code of function");
                for i in 0..code.len() {
                    println!("{}: {:?}", i, code[i]);
                }
            }
            let f = Function::from_basic_blocks(
                vec![BasicBlock { code: code, pc: 0 }],
                fun.clone().params,
            );
            let name = fun.name.clone();
            let name = match *name {
                Expr::Identifier(ref s) => s.clone(),
                Expr::GlobalIdent(ref s) => s.clone(),
                _ => unimplemented!(),
            };

            self.machine.create_global_object(name, Box::new(f));
        }

        for enumd in p.3.iter_mut() {
            let en = enumd.clone();
            let enum_ = Enum {
                name: en.clone().name,
                fields: RefCell::new(HashMap::new()),
            };

            for i in 0..enumd.fields.len() {
                let fields = en.clone().fields;
                enum_.fields.borrow_mut().insert(fields[i].clone(),i);
            }

            self.machine.create_global_object(enum_.name.clone(),Box::new(enum_));

        }

        for class in p.2.iter_mut() {
            let mut vclass = Class::new();

            let name = match *class.name {
                Expr::Identifier(ref i) => i.to_string(),
                _ => unimplemented!(),
            };
            vclass.name = name.clone();
            self.machine.create_global_object(&name, Box::new(vclass));
            for var in class.vars.iter_mut() {
                self.expr(&var.1);
                self.emit(Opcode::GetGlobal(name.clone()));
                self.emit(Opcode::SetField(var.0.clone()));
            }

            for fun in class.methods.clone().iter() {
                let mut cmpl = Compiler::new();
                let code = cmpl.compile((vec![*fun.clone().body], vec![], vec![],vec![]));
                
                if unsafe { DEBUG } {
                    println!("Printing code of function");
                    for i in 0..code.len() {
                        println!("{}: {:?}", i, code[i]);
                    }
                }
                let f = Function::from_basic_blocks(
                    vec![BasicBlock { code: code, pc: 0 }],
                    fun.clone().params,
                );


                let obj = self.machine.object_pool.allocate(Box::new(f));
                self.emit(Opcode::LoadObject(obj));
                self.emit(Opcode::GetGlobal(name.clone()));

                match *fun.name {
                    Expr::Identifier(ref name) => self.emit(Opcode::SetField(name.clone())),
                    _ => unimplemented!(),
                }
            }
        }

        for stmt in p.0 {
            self.stmt(stmt.clone());
        }

        self.finalize()
    }

    pub fn finalize(&mut self) -> Vec<Opcode> {
        builtin_fn::register(self);
        self.code
            .iter()
            .map(|e| match e {
                &UOP::Op(ref o) => o.clone(),
                &UOP::Goto(ref lbl) => Opcode::Goto(self.labels.get(lbl).unwrap().unwrap()),
            })
            .collect()
    }

    /// Sets the name of the label.
    pub fn label_here(&mut self, label: String) {
        *self.labels.get_mut(&label).unwrap() = Some(self.code.len());
    }

    pub fn emit(&mut self, op: Opcode) {
        self.code.push(UOP::Op(op));
    }
    pub fn emit_goto(&mut self, s: String) {
        self.code.push(UOP::Goto(s));
    }

    pub fn stmt(&mut self, s: Stmt) {
        match s {
            Stmt::Expr(e) => self.expr(&*e),
            Stmt::Block(b) => {
                for block in b.iter() {
                    self.stmt(block.clone());
                }
            }
            Stmt::Label(ref name) => {
                self.new_label_here(name.clone());
            }
            Stmt::Goto(ref name) => {
                self.emit_goto(name.clone());
            }
            Stmt::If(cond, block) => {
                self.expr(&*cond);
                let if_true = self.new_empty_label();
                let check = self.new_empty_label();
                let end = self.new_empty_label();
                self.emit_goto(check.clone());
                self.label_here(if_true.clone());
                self.stmt(*block);
                self.emit_goto(end.clone());

                let l = self.labels.clone();
                self.label_here(check);
                self.emit(Opcode::GotoIfTrue(l.get(&if_true).unwrap().unwrap()));
                self.label_here(end);
            }

            Stmt::IfElse(cond, t, f) => {
                self.expr(&*cond);
                let if_true = self.new_empty_label();
                let if_false = self.new_empty_label();
                let end = self.new_empty_label();
                let check = self.new_empty_label();
                self.emit_goto(check.clone());
                self.label_here(if_true.clone());
                self.stmt(*t);
                self.emit_goto(end.clone());

                self.label_here(if_false.clone());
                self.stmt(*f);
                self.emit_goto(end.clone());

                let l = self.labels.clone();
                self.label_here(check);
                self.emit(Opcode::GotoIfTrue(l.get(&if_true).unwrap().unwrap()));
                self.emit(Opcode::Goto(l.get(&if_false).unwrap().unwrap()));
                self.label_here(end);
            }

            Stmt::While(cond, block) => {
                let while_block = self.new_empty_label();
                let while_end = self.new_empty_label();
                let while_start = self.new_empty_label();

                self.last_end = while_end.clone();
                self.emit_goto(while_start.clone());
                self.label_here(while_block.clone());
                match *block {
                    Stmt::Block(stmts) => {
                        for stmt in stmts.iter() {
                            match stmt {
                                Stmt::Break => self.emit_goto(while_end.clone()),
                                v => self.stmt(v.clone()),
                            }
                        }
                    }
                    v => self.stmt(v),
                }

                self.label_here(while_start);
                self.expr(&*cond);
                let l = self.labels.clone();
                self.emit(Opcode::GotoIfTrue(
                    l.get(&while_block.clone()).unwrap().unwrap(),
                ));
                self.label_here(while_end);
            }

            Stmt::Return => {
                self.emit(Opcode::LoadNull);
                self.emit(Opcode::Return);
            }
            Stmt::Var(ref n, ref v) => match *v {
                Some(ref v) => {
                    self.expr(&*v);
                    self.emit(Opcode::SetGlobal(n.clone()));
                }
                None => {
                    self.emit(Opcode::LoadNull);
                    self.emit(Opcode::SetGlobal(n.clone()));
                }
            },

            Stmt::Local(ref n, ref v) => match *v {
                Some(ref v) => {
                    self.expr(&*v);
                    self.emit(Opcode::SetLocal(n.clone()));
                }
                None => {
                    self.emit(Opcode::LoadNull);
                    self.emit(Opcode::SetLocal(n.clone()));
                }
            },

            Stmt::ReturnWithVal(b) => {
                self.expr(&*b);
                self.emit(Opcode::Return);
            }

            Stmt::Break => {
                let end = self.last_end.clone();
                self.emit_goto(end);
            }
            _ => unimplemented!(),
        }
    }

    pub fn expr(&mut self, e: &Expr) {
        match e.clone() {
            Expr::Identifier(ref name) => {
                self.emit(Opcode::GetLocal(name.clone()));
            }
            Expr::FnCall(ref name, ref args) => {
                let mut name = name.clone();

                for arg in args.iter() {
                    self.expr(&arg);
                }
                if name.starts_with('$') {
                    name.remove(0);
                    self.emit(Opcode::GetGlobal(name.clone()));
                    self.emit(Opcode::GetGlobal(name.clone()));
                    self.emit(Opcode::Call(args.len()));
                } else {
                    self.emit(Opcode::GetGlobal(name.clone()));
                    self.emit(Opcode::GetLocal(name.clone()));
                    self.emit(Opcode::Call(args.len()));
                }
            }
            Expr::StringConst(ref s) => {
                self.emit(Opcode::LoadString(s.clone()));
            }

            Expr::FloatConst(f) => self.emit(Opcode::LoadDouble(f)),
            Expr::IntConst(i) => self.emit(Opcode::LoadLong(i)),
            Expr::True => self.emit(Opcode::LoadBool(true)),
            Expr::False => self.emit(Opcode::LoadBool(false)),
            Expr::Unit => self.emit(Opcode::LoadNull),
            Expr::This => self.emit(Opcode::LoadThis),
            Expr::Assignment(a, v) => {

                match *a {
                    Expr::Identifier(ref name) => {
                        self.expr(&*v);
                        self.emit(Opcode::SetLocal(name.clone()));
                    }
                    Expr::Index(ref name, ref e) => {
                        self.expr(&*e);
                        self.expr(&*v);
                        self.emit(Opcode::LoadString(String::from("__set__")));
                        self.emit(Opcode::GetLocal(name.to_string()));
                        self.emit(Opcode::GetLocal(name.clone()));
                        self.emit(Opcode::CallField(2));
                    }
                    Expr::GlobalIdent(ref name) => {
                        self.expr(&*v);
                        self.emit(Opcode::SetGlobal(name.clone()));
                    }
                    Expr::Dot(e, e2) => match (*e, *e2) {
                        (Expr::Identifier(ref sname), Expr::Identifier(ref name)) => {
                            self.expr(&*v);
                            if sname == "this" {
                                self.emit(Opcode::LoadThis);
                                self.emit(Opcode::SetField(name.clone()));
                            } else {
                                self.emit(Opcode::GetLocal(sname.clone()));
                                self.emit(Opcode::SetField(name.clone()));
                            }
                        }
                        (Expr::GlobalIdent(ref sname), Expr::Identifier(ref name)) => {
                            self.expr(&*v);
                            let mut sname = sname.clone();
                            if sname.starts_with('$') {
                                sname.remove(0);
                            }
                            self.emit(Opcode::GetGlobal(sname));
                            self.emit(Opcode::SetField(name.to_string()))
                        }
                        (Expr::This, Expr::Identifier(ref name)) => {
                            self.expr(&*v);
                            self.emit(Opcode::LoadThis);
                            self.emit(Opcode::SetField(name.clone()));
                        }
                        (ref e,Expr::Identifier(ref n)) => {
                            self.expr(&v);
                            self.expr(e);

                            self.emit(Opcode::SetField(n.to_string()));

                        }
                        v => panic!("Index {:?}", v),
                    },
                    v => panic!("Unimplemented(Index) {:?}", v),
                }
            }
            Expr::Array(ref v) => {
                for value in v.iter() {
                    self.expr(value);
                }

                self.emit(Opcode::GetGlobal("new_array".into()));
                self.emit(Opcode::Dup);
                self.emit(Opcode::Call(v.len()));

            }

            Expr::Dot(dot_lhs, dot_rhs) => {
                //println!("{:?} {:?}",dot_lhs,dot_rhs);
                match (*dot_lhs, *dot_rhs) {
                    (Expr::GlobalIdent(ref sname), Expr::FnCall(ref name, ref args)) => {
                        let mut sname = sname.clone();
                        if sname.starts_with('$') {
                            sname.remove(0);
                        }
                        for arg in args.iter() {
                            self.expr(&arg);
                        }
                        self.emit(Opcode::LoadString(name.clone()));
                        self.emit(Opcode::GetGlobal(sname.clone()));
                        self.emit(Opcode::GetGlobal(sname.clone()));
                        self.emit(Opcode::CallField(args.len()));
                    }
                    (Expr::Identifier(ref sname), Expr::Identifier(ref name)) => {
                        self.emit(Opcode::GetLocal(sname.clone()));
                        self.emit(Opcode::GetField(name.clone()));
                    }
                    (Expr::GlobalIdent(ref sname), Expr::Identifier(ref name)) => {
                        let mut sname = sname.clone();
                        if sname.starts_with('$') {
                            sname.remove(0);
                        }
                        self.emit(Opcode::GetGlobal(sname.clone()));
                        self.emit(Opcode::GetField(name.clone()));
                    }
                    (Expr::Identifier(ref sname), Expr::FnCall(ref name, ref args)) => {
                        let mut sname = sname.clone();
                        if sname.starts_with('$') {
                            sname.remove(0);
                        }
                        for arg in args.iter() {
                            self.expr(&arg);
                        }
                        self.emit(Opcode::LoadString(name.clone()));
                        self.emit(Opcode::GetLocal(sname.clone()));
                        self.emit(Opcode::GetLocal(sname.clone()));
                        self.emit(Opcode::CallField(args.len()));
                    }

                    (Expr::GlobalIdent(ref sname), Expr::Dot(ref name, ref another)) => {
                        let mut sname = sname.clone();
                        if sname.starts_with('$') {
                            sname.remove(0);
                        }

                        let name = match **name {
                            Expr::Identifier(ref s) => s.clone(),
                            _ => unimplemented!(),
                        };
                        match **another {
                            Expr::FnCall(ref fname, ref args) => {
                                for argument in args.iter() {
                                    self.expr(argument);
                                }

                                self.emit(Opcode::LoadString(fname.clone()));
                                self.emit(Opcode::LoadNull);
                                self.emit(Opcode::GetGlobal(sname));
                                self.emit(Opcode::GetField(name));
                                self.emit(Opcode::CallField(args.len()));
                            }
                            Expr::Identifier(ref iname) => {
                                self.emit(Opcode::GetGlobal(sname));
                                self.emit(Opcode::GetField(name));
                                self.emit(Opcode::GetField(iname.to_string()));
                            }

                            _ => unimplemented!(),
                        }
                    }
                    (Expr::This, Expr::Identifier(ref name)) => {
                        self.emit(Opcode::LoadThis);
                        self.emit(Opcode::GetField(name.clone()));
                    }

                    (Expr::Identifier(ref sname), Expr::Dot(ref e, ref mut e2)) => {
                        let mut sname = sname.clone();
                        if sname.starts_with('$') {
                            sname.remove(0);
                        }

                        let e = *e.clone();
                        let e2 = *e2.clone();
                        match (e, e2) {
                            (Expr::Identifier(ref name), Expr::FnCall(ref fname, ref args)) => {
                                for arg in args.iter() {
                                    self.expr(arg);
                                }
                                self.emit(Opcode::LoadString(fname.to_string()));
                                self.emit(Opcode::GetLocal(sname.to_string()));
                                self.emit(Opcode::GetField(name.to_string()));
                                self.emit(Opcode::Dup);
                                self.emit(Opcode::CallField(args.len()));
                            }

                            _ => unimplemented!(),
                        }
                    }
                    (Expr::This, Expr::FnCall(ref fname, ref args)) => {
                        for argument in args.iter() {
                            self.expr(argument);
                        }
                        self.emit(Opcode::LoadString(fname.to_string()));
                        self.emit(Opcode::LoadThis);
                        self.emit(Opcode::Dup);
                        self.emit(Opcode::CallField(args.len()));
                    }
                    (ref e, Expr::Identifier(ref n)) => {
                        self.expr(e);
                        self.emit(Opcode::Dup);
                        self.emit(Opcode::GetField(n.to_string()));
                    }

                    (Expr::Index(ref v,ref idx),Expr::FnCall(ref fname,ref args)) => {
                        for arg in args.iter() {
                            self.expr(arg);
                        }
                        self.emit(Opcode::LoadString(fname.to_string()));
                        self.expr(&Expr::Index(v.clone(),idx.clone()));
                        self.emit(Opcode::Dup);

                        self.emit(Opcode::CallField(args.len()));

                    }
                    (v, v2) => panic!("Unimplemented {:?} {:?}", v, v2),
                }
            }

            Expr::Index(ref name, ref idx) => {
                self.expr(&*idx);
                self.emit(Opcode::LoadString(String::from("__get__")));
                self.emit(Opcode::GetLocal(name.clone()));
                self.emit(Opcode::Dup);
                self.emit(Opcode::CallField(1));
            }

            Expr::GlobalIdent(ref name) => {
                let mut name = name.clone();
                if name.starts_with('$') {
                    name.remove(0);
                }
                self.emit(Opcode::GetGlobal(name.clone()));
            }

            v => panic!("{:?}", v),
        }
    }
}
