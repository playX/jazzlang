pub mod builtin_fn;
pub mod codegen;
pub mod parser;
pub mod classes;

pub static mut DEBUG: bool = false;

pub fn set_debug(b: bool) {
    unsafe { DEBUG = b }
}