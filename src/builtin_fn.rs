use super::codegen::Compiler;
use jazz_vm::*;
use std::cell::RefCell;
use std::collections::HashMap;
pub use super::classes::*;


fn clone(m: &mut Machine, args: Vec<Value>) -> Value {
    let ctx = ValueContext::new(&args[0], &m.object_pool).as_object();
    ctx.o_clone(&mut m.object_pool)
}

fn new_array(m: &mut Machine,args: Vec<Value>) -> Value {
    let mut values = vec![];
    for argument in args.iter() {
        values.push(*argument);
    }

    let mut array = Array::new();
    array.elements = RefCell::new(values);
    let object = m.object_pool.allocate(Box::new(array));
    Value::Object(object)
}

fn concat(m: &mut Machine, args: Vec<Value>) -> Value {
    let mut string = String::new();
    for arg in args.iter() {
        let obj = ValueContext::new(&arg, &m.object_pool).as_object();
        string += &obj.toString();
    }
    let obj = m.object_pool.allocate(Box::new(string));
    return Value::Object(obj);
}

pub fn println(m: &mut Machine, args: Vec<Value>) -> Value {
    for obj in args.iter() {
        match obj {
            Value::Bool(b) => {
                print!("{}", b);
            }
            Value::Object(id) => {
                let value = ValueContext::new(&Value::Object(*id), &m.object_pool)
                    .as_object()
                    .toString();
                print!("{}", value);
            }
            Value::Null => print!("null"),
            Value::Int(i) => print!("{}", i),
            v => panic!("{:?}", v),
        };
    }
    println!();
    Value::Null
}

pub fn print(m: &mut Machine, args: Vec<Value>) -> Value {
    for obj in args.iter() {
        match obj {
            Value::Bool(b) => {
                print!("{}", b);
            }
            Value::Object(id) => {
                let value = ValueContext::new(&Value::Object(*id), &m.object_pool)
                    .as_object()
                    .toString();
                print!("{}", value);
            }
            Value::Null => print!("null"),
            Value::Int(i) => print!("{}", i),
            v => panic!("{:?}", v),
        };
    }

    Value::Null
}

pub fn int(m: &mut Machine,args: Vec<Value>) -> Value {
    let obj = args[0];
        match obj {
            Value::Bool(b) => {
                let idx = m.object_pool.allocate(Box::new(b.clone() as i64));
                return Value::Object(idx)
            }
            Value::Int(i) => {
                let idx = m.object_pool.allocate(Box::new(
                    i.clone() as i64));
                return Value::Object(idx)
            }
            Value::Float(f) => {
                let idx = m.object_pool.allocate(Box::new(f.clone() as f64));
                return Value::Object(idx)
            }
            Value::Object(idx) => {
                let i = ValueContext::new(&Value::Object(idx),&m.object_pool).as_object().to_i64();
                return Value::Object(m.object_pool.allocate(Box::new(i)))
            }
            _ => unimplemented!(),
        }

}



pub fn register(cmpl: &mut Compiler) {
    //let print = Function::from_native(Box::new(print));
    let math_class = Box::new(MathClass {
        fields: RefCell::new(HashMap::new()),
    });
    cmpl.machine.create_global_object("Math", math_class);
    cmpl.machine.create_global_object("new_array",Box::new(Function::from_native(Box::new(new_array))));
    cmpl.machine.create_global_object("int",Box::new(Function::from_native(Box::new(int))));
    //cmpl.machine.create_global_object("Arr", Box::new(Array::new()));
    let system_class = Box::new(SystemClass::new());
    cmpl.machine.create_global_object("System", system_class);
    cmpl.machine
        .create_global_object("concat", Box::new(Function::from_native(Box::new(concat))));
    cmpl.machine
        .create_global_object("clone", Box::new(Function::from_native(Box::new(clone))));
    /*cmpl.machine.create_global_object("print", Box::new(print));*/

    binop(cmpl);
    cmp(cmpl);
}

pub fn binop(cmpl: &mut Compiler) {
    let badd = Function::from_basic_blocks(
        vec![BasicBlock {
            code: vec![
                Opcode::GetLocal("x".into()),
                Opcode::GetLocal("y".into()),
                Opcode::Add,
                Opcode::Return,
            ],
            pc: 0,
        }],
        vec!["x".into(), "y".into()],
    );

    cmpl.machine.create_global_object("+", Box::new(badd));
    let badd = Function::from_basic_blocks(
        vec![BasicBlock {
            code: vec![
                Opcode::GetLocal("x".into()),
                Opcode::GetLocal("y".into()),
                Opcode::Sub,
                Opcode::Return,
            ],
            pc: 0,
        }],
        vec!["x".into(), "y".into()],
    );

    cmpl.machine.create_global_object("-", Box::new(badd));
    let badd = Function::from_basic_blocks(
        vec![BasicBlock {
            code: vec![
                Opcode::GetLocal("x".into()),
                Opcode::GetLocal("y".into()),
                Opcode::Mul,
                Opcode::Return,
            ],
            pc: 0,
        }],
        vec!["x".into(), "y".into()],
    );

    cmpl.machine.create_global_object("*", Box::new(badd));
    let badd = Function::from_basic_blocks(
        vec![BasicBlock {
            code: vec![
                Opcode::GetLocal("x".into()),
                Opcode::GetLocal("y".into()),
                Opcode::Div,
                Opcode::Return,
            ],
            pc: 0,
        }],
        vec!["x".into(), "y".into()],
    );

    cmpl.machine.create_global_object("/", Box::new(badd));
}

fn cmp(cmpl: &mut Compiler) {
    let badd = Function::from_basic_blocks(
        vec![BasicBlock {
            code: vec![
                Opcode::GetLocal("y".into()),
                Opcode::GetLocal("x".into()),
                Opcode::Eq,
                Opcode::Return,
            ],
            pc: 0,
        }],
        vec!["x".into(), "y".into()],
    );

    cmpl.machine.create_global_object("==", Box::new(badd));
    let badd = Function::from_basic_blocks(
        vec![BasicBlock {
            code: vec![
                Opcode::GetLocal("y".into()),
                Opcode::GetLocal("x".into()),
                Opcode::Eq,
                Opcode::Not,
                Opcode::Return,
            ],
            pc: 0,
        }],
        vec!["x".into(), "y".into()],
    );

    cmpl.machine.create_global_object("!=", Box::new(badd));
    let badd = Function::from_basic_blocks(
        vec![BasicBlock {
            code: vec![
                Opcode::GetLocal("y".into()),
                Opcode::GetLocal("x".into()),
                Opcode::Gt,
                Opcode::Return,
            ],
            pc: 0,
        }],
        vec!["x".into(), "y".into()],
    );

    cmpl.machine.create_global_object(">", Box::new(badd));
    let badd = Function::from_basic_blocks(
        vec![BasicBlock {
            code: vec![
                Opcode::GetLocal("y".into()),
                Opcode::GetLocal("x".into()),
                Opcode::Lt,
                Opcode::Return,
            ],
            pc: 0,
        }],
        vec!["x".into(), "y".into()],
    );
    cmpl.machine.create_global_object("<", Box::new(badd));
    let badd = Function::from_basic_blocks(
        vec![BasicBlock {
            code: vec![
                Opcode::GetLocal("y".into()),
                Opcode::GetLocal("x".into()),
                Opcode::Ge,
                Opcode::Return,
            ],
            pc: 0,
        }],
        vec!["x".into(), "y".into()],
    );

    cmpl.machine.create_global_object(">=", Box::new(badd));
    let badd = Function::from_basic_blocks(
        vec![BasicBlock {
            code: vec![
                Opcode::GetLocal("y".into()),
                Opcode::GetLocal("x".into()),
                Opcode::Le,
                Opcode::Return,
            ],
            pc: 0,
        }],
        vec!["x".into(), "y".into()],
    );

    cmpl.machine.create_global_object("<=", Box::new(badd));
    let badd = Function::from_basic_blocks(
        vec![BasicBlock {
            code: vec![
                Opcode::GetLocal("y".into()),
                Opcode::GetLocal("x".into()),
                Opcode::Or,
                Opcode::Return,
            ],
            pc: 0,
        }],
        vec!["x".into(), "y".into()],
    );

    cmpl.machine.create_global_object("||", Box::new(badd));
    let badd = Function::from_basic_blocks(
        vec![BasicBlock {
            code: vec![
                Opcode::GetLocal("y".into()),
                Opcode::GetLocal("x".into()),
                Opcode::And,
                Opcode::Return,
            ],
            pc: 0,
        }],
        vec!["x".into(), "y".into()],
    );

    cmpl.machine.create_global_object("&&", Box::new(badd));
}
