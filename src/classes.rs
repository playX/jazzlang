use jazz_vm::*;
use std::cell::RefCell;
use std::collections::HashMap;
use super::builtin_fn::*;

extern crate rand;

#[derive(Debug)]
pub struct Array {
    pub elements: RefCell<Vec<Value>>,
}

impl Array {
    pub fn new() -> Array {
        Array {
            elements: RefCell::new(Vec::new()),
        }
    }
}

impl Object for Array {
    fn o_clone(&self,p: &mut ObjectPool) -> Value {
        let array = Array {
            elements: self.elements.clone(),
        };
        Value::Object(p.allocate(Box::new(array)))
    }
    fn get_children(&self) -> Vec<usize> {
        self.elements
            .borrow()
            .iter()
            .filter(|v| v.is_object())
            .map(|v| v.as_object_id())
            .collect()
    }

    fn as_any(&self) -> &Any {
        self as &Any
    }

    fn as_any_mut(&mut self) -> &mut Any {
        self as &mut Any
    }

    fn toString(&self) -> String {
        format!("Array({})", self.elements.borrow().len())
    }

    fn call_field(&self, name: &str, executor: &mut Machine, _argc: Option<usize>) -> Value {
        match name {
            "__get__" | "get" => {
                let index = executor.last_frame().arguments.last().unwrap();
                let index = ValueContext::new(&index, executor.get_object_pool()).to_i64() as usize;
                let elements = self.elements.borrow();
                if index >= elements.len() {
                    panic!("Array index out of bound")
                }
                elements[index]
            }
            "__set__" | "set" => {
                let index = executor.last_frame().arguments[1];
                let val = executor.last_frame().arguments[0];

                let index = ValueContext::new(&index, executor.get_object_pool()).to_i64() as usize;
                let len = self.elements.borrow().len();
                if index >= len {
                    panic!("Array index out of bound")
                }
                self.elements.borrow_mut()[index] = val;

                Value::Null
            }
            "push" | "append" => {
                let val = executor.last_frame().arguments.last().unwrap();
                self.elements.borrow_mut().push(val.clone());
                Value::Null
            }
            "toString" => {
                let mut string = String::new();
                let len = self.elements.borrow().len();
                string += "[";
                for i in 0..len {
                    let ctx = ValueContext::new(&self.elements.borrow()[i as usize],&executor.object_pool).as_object().toString();
                    string += &ctx;
                    string += ",";
                }
                string += "]";
                let string = executor.object_pool.allocate(Box::new(string));
                Value::Object(string)
            }
            "pop" => self
                .elements
                .borrow_mut()
                .pop()
                .unwrap_or_else(|| panic!("No elements")),
            "__len__" | "len" | "size" => {
                let int = self.elements.borrow().len() as i64;
                let int = executor.object_pool.allocate(Box::new(int));
                Value::Object(int)
            },
            _ => panic!("field not found `{}`", name),
        }
    }
}

#[derive(Clone)]
pub struct SystemClass {
    pub fields: RefCell<HashMap<String, Value>>,
}

#[derive(Clone)]
pub struct MathClass {
    pub fields: RefCell<HashMap<String, Value>>,
}

impl Object for MathClass {
    fn initialize(&mut self, _pool: &mut jazz_vm::ObjectPool) {
        fn sqrt(m: &mut Machine, args: Vec<Value>) -> Value {
            if args.len() > 1 {
                panic!("Expected 1 argument!");
            }

            let value = args[0];
            let ctx = ValueContext::new(&value, &m.object_pool).as_object();
            let float = ctx.to_f64();

            let result = float.sqrt();

            let result = m.object_pool.allocate(Box::new(result));
            Value::Object(result)
        }

        fn tan(m: &mut Machine,args: Vec<Value>) -> Value {
            if args.len() > 1 {
                panic!("Expected 1 argument!");
            }
            let value = args[0];
            let ctx = ValueContext::new(&value,&m.object_pool).as_object();
            
                let f = ctx.to_f64().tan();
                let v = Value::Object(m.object_pool.allocate(Box::new(f)));
                v
            
        }
        
        let tanf = Function::from_native(Box::new(tan));
        let id = _pool.allocate(Box::new(tanf));
        self.fields
            .borrow_mut()
            .insert("tan".to_string(),Value::Object(id));
        let sqrt = Function::from_native(Box::new(sqrt));
        let id = _pool.allocate(Box::new(sqrt));
        self.fields
            .borrow_mut()
            .insert("sqrt".to_string(), Value::Object(id));
    }

    fn get_children(&self) -> Vec<usize> {
        Vec::new()
    }
    fn o_clone(&self, p: &mut ObjectPool) -> Value {
        let v = p.allocate(Box::new(self.clone()));
        Value::Object(v)
    }
    fn call_field(&self, field_name: &str, executor: &mut Machine, _argc: Option<usize>) -> Value {
        
        if field_name != "rand_int" && field_name != "rand_float" {
            let fields = self.fields.borrow();
            let field = fields
                .get(field_name)
                .expect(&format!("No field with name `{}`", field_name));
            let object = ValueContext::new(field, &executor.object_pool).as_object();

            object.call(executor, _argc)
        } else {
            match field_name {
                "rand_int" => {
                    let int = self::rand::random::<i64>();
                    let v = Value::Object(executor.object_pool.allocate(Box::new(int)));
                    v
                }
                "rand_float" => {
                    let float = self::rand::random::<f64>();
                    let v = Value::Object(executor.object_pool.allocate(Box::new(float)));
                    v
                }
                _ => unreachable!(),
            }
        }
        

    }

    fn get_field(&self, _pool: &ObjectPool, _name: &str) -> Option<Value> {
        let fields = self.fields.borrow();
        let field = fields
            .get(_name)
            .expect(&format!("No field with name `{}`", _name));
        Some(*field)
    }

    fn set_field(&self, _name: &str, _value_ref: Value) {
        panic!("Cannot set fields on System class!");
    }

    fn as_any(&self) -> &Any {
        self as &Any
    }
    fn as_any_mut(&mut self) -> &mut Any {
        self as &mut Any
    }
}
use std::any::Any;

impl SystemClass {
    pub fn new() -> SystemClass {
        SystemClass {
            fields: RefCell::new(HashMap::new()),
        }
    }
}

impl Object for SystemClass {
    fn initialize(&mut self, _pool: &mut jazz_vm::ObjectPool) {
        let print = Function::from_native(Box::new(print));
        let id = _pool.allocate(Box::new(print));
        let println = Function::from_native(Box::new(println));
        let printlnid = _pool.allocate(Box::new(println));
        self.fields
            .borrow_mut()
            .insert("println".to_string(), Value::Object(printlnid));
        self.fields
            .borrow_mut()
            .insert("print".to_string(), Value::Object(id));
    }

    fn get_children(&self) -> Vec<usize> {
        Vec::new()
    }
    fn o_clone(&self, p: &mut ObjectPool) -> Value {
        let v = p.allocate(Box::new(self.clone()));
        Value::Object(v)
    }
    fn call_field(&self, field_name: &str, executor: &mut Machine, _argc: Option<usize>) -> Value {
        let fields = self.fields.borrow();
        let field = fields
            .get(field_name)
            .expect(&format!("No field with name `{}`", field_name));
        let object = ValueContext::new(field, &executor.object_pool).as_object();

        object.call(executor, _argc)
    }

    fn get_field(&self, _pool: &ObjectPool, _name: &str) -> Option<Value> {
        let fields = self.fields.borrow();
        let field = fields
            .get(_name)
            .expect(&format!("No field with name `{}`", _name));
        Some(*field)
    }

    fn set_field(&self, _name: &str, _value_ref: Value) {
        panic!("Cannot set fields on System class!");
    }

    fn as_any(&self) -> &Any {
        self as &Any
    }
    fn as_any_mut(&mut self) -> &mut Any {
        self as &mut Any
    }
}


