extern crate jazz_vm;
extern crate jazz_lang;
use jazz_lang::*;
use jazz_lang::codegen::*;


use jazz_vm::*;
use std::fs::File;
use std::io::prelude::*;
use std::path::PathBuf;
use structopt::StructOpt;



extern crate structopt;

#[derive(StructOpt, Debug)]
struct Options {
    #[structopt(name = "FILE", parse(from_os_str))]
    file: Option<PathBuf>,
    #[structopt(short = "d", long = "debug")]
    debug: bool,
}

fn main() {
    let mut src = String::new();

    let ops = Options::from_args();

    if let Some(path) = ops.file {
        File::open(path).unwrap().read_to_string(&mut src).unwrap();
    } else {
        panic!("You should enter file path");
    }

    set_debug(ops.debug);

    let mut peekable = parser::lex(&src).peekable();

    let parsed_tree = parser::parse(&mut peekable).unwrap();

    let mut compiler = Compiler::new();

    let mut opcodes = compiler.compile(parsed_tree);

    opcodes.push(Opcode::LoadNull);
    opcodes.push(Opcode::Return);

    if ops.debug {
        println!("Printing generated code: ");
        let mut i = 0;
        while i < opcodes.len() {
            println!("{}: {:?}", i, opcodes[i]);
            i += 1;
        }
    }

    let mut machine = compiler.machine;
    let main = Function::from_basic_blocks(
        vec![BasicBlock {
            code: opcodes,
            pc: 0,
        }],
        vec![],
    );
    machine.create_global_object("main", Box::new(main));
    let f = machine.get_global_object("main").unwrap().clone();
    machine.invoke(f.clone(), Value::Null, None, &[]);
}
